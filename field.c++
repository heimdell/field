
#ifndef FIELD_HPP
#define FIELD_HPP

#include "trace.h++"
#include "sizes.h++"

template <class Host_, int offset, int size>
struct Field
{
	typedef          Host_                  Host;
	typedef typename typeOfSize<size>::type Cell;

	Field(Host &host) {
		point = ptr<Cell>(ptr<char>(&host) + offset);
	}

	Field &operator = (Cell value) { *point = value; return *this; }

	operator Cell() { return *point; }

private:
	Cell * point;

	template <class T> 
	static T * ptr(void * address) { return (T *) address; }
};

#define INHERIT_TYPES_FROM(parent) \
	typedef typename parent::Host Host; \
	typedef typename parent::Cell Cell

#define WRAPPING_CTOR(type, field) \
	type(Host &host) \
	  : field(host) \
	{ }

#include "endianness.h++"

template <class Field>
struct BigEndian { 
	INHERIT_TYPES_FROM(Field);
	WRAPPING_CTOR(BigEndian, field);

	BigEndian &operator = (Cell value) { field = toggle(value); return *this; }

	operator Cell () { return toggle(field); }

private:
	Field field;

	static Cell toggle(Cell value) { 
		return toggleEndianness<sizeof(Cell)> (value); 
	}
};


#include "shifts.h++"

template <class Field, int mask_>
struct Masked {
	INHERIT_TYPES_FROM(Field);
	WRAPPING_CTOR(Masked, field);

	ASSIGN(mask,  mask_);
	ASSIGN(shift, shift_from_mask<mask>::value);
	
	Masked &operator = (Cell value)	{

		field = (field & ~mask) | ((value << shift) & mask);

		return *this;
	}

	operator Cell() { return (field & mask) >> shift; }

private:
	Field field;
};

constexpr int bit(const int n) { return 1 << n; }

template <class Field, int n>
struct Flag {
	INHERIT_TYPES_FROM(Field);
	WRAPPING_CTOR(Flag, field);

	ASSIGN(mask, bit(n));

	Flag &operator = (bool flag) { field = flag; return *this; }

	operator bool() { return field; }

private:
	Masked<Field, mask> field;
};

template <class Field>
struct BitSet {
	INHERIT_TYPES_FROM(Field);
	WRAPPING_CTOR(BitSet, field)

	BitSet &operator = (Cell state) { field = state; return *this; }

	operator Cell()     { return field; }

	bool get(Cell flag) { return field & flag; }

	BitSet &set    (Cell flag = -1) { field = field |  flag; return *this; }
	BitSet &clear  (Cell flag = -1) { field = field & ~flag; return *this; }

	bool any(Cell flagset) { return  flagset & field;             }
	bool all(Cell flagset) { return (flagset & field) == flagset; }

private:
	Field field;
};

template <class Host, int offset> using Byte  = Field<Host, offset, 1>;
template <class Host, int offset> using Word  = Field<Host, offset, 2>;
template <class Host, int offset> using DWord = Field<Host, offset, 4>;

template <class Host>
struct Wrapper
{
	Host host;

	Wrapper() { }

	Wrapper(Host host) : host(host) { }

	template <class Field>
	Wrapper &the(typename Field::Cell value) {
		(Field (*this)) = value;

		return *this;
	}

	template <class Field>
	Field the() {
		return Field (*this);
	}
};

int main() {

	typedef Wrapper<unsigned long> Master;

	typedef Masked<BigEndian<Word<Master, 1>>, 0x0FF0> Test;

	Master master = 0;

	head("Testing the basic field get/set"); {

		say(master.the<Test>() = 42);
		say(master.the<Test>(43));

		produces;

		trace((int) master.the<Test>());
	}

	typedef BigEndian<Word<Master, 1>> Base;

	typedef Flag<Base, 5> TestOne;

	typedef Flag<Base, 6> TestZero;

	head("Testing the flags"); {

		say(master.the<TestOne> () = 1);
		say(master.the<TestZero>() = 0);

		produces;

		trace(master.the<TestOne>());
		trace(master.the<TestZero>());
	}

	head("Testing the length of mask"); {

		trace(length_of_mask<0xFF0>::value);

	}

	head("Testing the bitfields"); {

		enum Bits {
			First  = bit(1),
			Second = bit(2),
			Third  = bit(3),
			Fourth = bit(4),
		};

		typedef BitSet<Base> BitField;

		auto bitField = master.the<BitField>();

		say(bitField.set  (First  | Third | Fourth));
		say(bitField.clear(Second | Third));

		produces;

		trace(bitField.get(First));
		trace(bitField.get(Second));
		trace(bitField.get(Third));
		trace(bitField.get(Fourth));

		trace(master.the<BitField>().any(Second | Third));
		trace(master.the<BitField>().any(Second | First));

		trace(master.the<BitField>().all(Second | First));
		trace(master.the<BitField>().all(Fourth | First));

	}
}

#endif