
#ifndef ENDIANNESS_HPP
#define ENDIANNESS_HPP

#include <algorithm>

using namespace std;

template <int size> int toggleEndianness(int x);

static bool is_big_endian();

template <>
int toggleEndianness<1>(int x) { return x; }

template <>
int toggleEndianness<2>(int x) { 
	static bool already = is_big_endian();

	if (already) return x;

	union { short s; char c[2]; } conv = { (short) x };

	swap(conv.c[0], conv.c[1]);

	return conv.s;
}

template <>
int toggleEndianness<4>(int x) { 
	static bool already = is_big_endian();

	if (already) return x;

	union { long l; char c[4]; } conv = { x };

	swap(conv.c[0], conv.c[3]);
	swap(conv.c[1], conv.c[2]);

	return conv.l;
}

static bool is_big_endian()
{
    union {
        unsigned long i;
        char          c[4];
    } bint = { 0x01020304 };

    return bint.c[0] == 1; 
}

#endif