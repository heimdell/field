
#ifndef MACROSES
#define MACROSES

#define VALUE(x) enum { value = (x) }

#define TYPE(t)  typedef t type

#define ASSIGN(name, value) enum { name = value }

#endif