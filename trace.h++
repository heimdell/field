
#ifndef TRACE_HPP
#define TRACE_HPP

#include <iostream>

using namespace std;

#define line(x)   cout << x << line;
#define trace(x)  cout << #x " == " << x << endl;
#define say(x)    cout << #x ";" << endl; x;

#define produces  cout << " =>" << endl;

#define head(msg) cout << endl << "---- " << msg << " ----" << endl;

#endif