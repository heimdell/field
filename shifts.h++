
#ifndef SHIFTS_HPP
#define SHIFTS_HPP

#include "macroses.h++"

template <int> struct shift_from_mask;
template <int> struct length_of_mask;

template <bool flag, class A, class B> struct If;
template <           class A, class B> struct If<true,  A, B> { TYPE(A); };
template <           class A, class B> struct If<false, A, B> { TYPE(B); };

// stub, because templates evaluate eagerly
template <>
struct shift_from_mask<0> { VALUE(-1); };

template <int mask>
struct shift_from_mask {

	struct init { VALUE(-1); };

	typedef typename 
		If  < mask % 2 == 1
			, init
		// Else
			, shift_from_mask<mask / 2>
		
		>::type bearer;

	VALUE(bearer::value + 1);
};

template <> struct length_of_mask<0> { VALUE(0); };

template <int mask> 
struct length_of_mask { 
	VALUE(mask % 2 + length_of_mask<mask / 2>::value); 
};


#endif